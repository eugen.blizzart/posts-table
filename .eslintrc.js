module.exports = {
  root: true,
  extends: ["plugin:vue/vue3-recommended"],
  rules: {
    "vue/no-v-html": "off",
  },
};
