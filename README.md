# posts-table

## Project setup

```
pnpm install
```

### Compiles and hot-reloads for development

```
pnpm run serve
```

### Compiles and minifies for production

```
pnpm run build
```

### Lints and fixes files

```
pnpm run lint
```

For autosave add in settings.json (VS Code):

```
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "eslint.validate": ["javascript"],
```

Поэксперементировал с Suspense(ветка noprod). Хотел заюзать композоблы из vue-use, но передумал.
Со стилями и иконками не стал заморачиваться и сделал как прототип.

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

```

```
